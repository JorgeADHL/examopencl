#include <iostream>
#include <boost/compute.hpp>
#include <boost/math/constants/constants.hpp>
#include <vector>
#include <random>
#include <chrono>
#include<fstream>

using namespace std;
namespace bc = boost::compute;

//void showMatrix(vector<float> *matrix, unsigned int matrixRows, unsigned int matrixCols);
vector<float> *createSinewaveSignal(
       const unsigned int pps,
       const unsigned int points,
       const float peakAmplitude,
       const float frequency
);
vector<float> *addWhiteNoise(
       vector<float> *signal,
       const float standardDeviation
);
vector<float> *centeredMovingAverage(
       vector<float> *signal,
       unsigned int points
);
void writeSignalCSV(
       vector<float> *signal,
       const string filename,
       const unsigned int pps
);


int main() {

	const unsigned int pps=1800;
	unsigned int points=5400;
	float peakAmplitude=100.0f;
	float frequency=2.0f;
	float standardDeviation=10.0f;

	   vector<float> *mySignal = createSinewaveSignal(pps,points,peakAmplitude,frequency);
	   addWhiteNoise(mySignal,standardDeviation);
	   writeSignalCSV(mySignal,"originalSignal.csv",pps);
	   vector<float> *cma3 = centeredMovingAverage(mySignal,3);
	   writeSignalCSV(cma3,"cma3.csv",pps);
	   vector<float> *cma5 = centeredMovingAverage(mySignal,5);
	   writeSignalCSV(cma5,"cma5.csv",pps);
	   vector<float> *cma21 = centeredMovingAverage(mySignal,21);
	   writeSignalCSV(cma21,"cma21.csv",pps);
	   return 0;
	}

vector<float> *createSinewaveSignal(
       const unsigned int pps,
       const unsigned int points,
       const float peakAmplitude,
       const float frequency){

	float pi = boost::math::constants::pi<float>();
	float omega=2.0f*pi*frequency;
	vector<float> *signal = new vector<float>(points,0.0f);
	for(unsigned int i=0;i<points;i++){
	float t=(float)i/(float)pps;
	 (*signal)[i]=peakAmplitude*sin(omega*t);
	}
	return signal;
}

vector<float> *addWhiteNoise(
       vector<float> *signal,
       const float standardDeviation){
	default_random_engine eng;
	normal_distribution<float> norm(0.0f,standardDeviation);
	unsigned int datasize=signal->size();
	for(unsigned int i; i<datasize;i++){
		(*signal)[i]+=norm(eng);
	}
	return signal;
}

vector<float> *centeredMovingAverage(
       vector<float> *signal,
       unsigned int points){

	const char programSourceCode[] = BOOST_COMPUTE_STRINGIZE_SOURCE(
	// Put your code here
	        __kernel void cmaKernel(
	                __global float *signal,
	                unsigned int pointsCma,
	                unsigned int size,
					__global float *signalOutput) {
	            unsigned int p = get_global_id(0);
	            unsigned int m=(pointsCma)/2;
	            if(p<m||p+m>=size){
	            	signalOutput[p]=signal[p];
	            }
	            else
	            	{
	            	float accum=0.0f;
	            			for(unsigned int i=p-m;i<=p+m;i++){
	            				accum+=signal[i];
	            			}
	            	signalOutput[p]=accum/(float)pointsCma;
	            	}
	            return;
	        }
	        );

	 	 bc::device oclDevice =
	            bc::system::default_device();
	    bc::context oclContext =
	            bc::context(oclDevice);
	    bc::command_queue oclQueue =
	            bc::command_queue(oclContext,oclDevice);
	    bc::program oclProgram =
	            bc::program::create_with_source(programSourceCode, oclContext);
	    oclProgram.build();
	    bc::kernel oclRareKernel = oclProgram.create_kernel("cmaKernel");

	    const unsigned int vectorSize = signal->size();
	    vector<float> *h_signalOutput=new vector<float>(vectorSize,0.0f);

	    bc::vector<float> d_signal((*signal).begin(),(*signal).end(),oclQueue);
	    bc::vector<float> d_signalOutput(vectorSize,0.0f,oclQueue);

	    oclRareKernel.set_arg(0, d_signal);
	    oclRareKernel.set_arg(1, points);
	    oclRareKernel.set_arg(2, vectorSize);
	    oclRareKernel.set_arg(3, d_signalOutput);
	    size_t workDims[1] = {vectorSize};
	    oclQueue.enqueue_nd_range_kernel(
	            oclRareKernel,
	            1,
	            nullptr,
	            workDims,
	            nullptr);

	    bc::copy(d_signalOutput.begin(),d_signalOutput.end(),h_signalOutput->begin(),oclQueue);

	    return h_signalOutput;

}

void writeSignalCSV(
       vector<float> *signal,
       const string filename,
       const unsigned int pps){

	ofstream fs(filename);
	fs<< "time,data"<<endl;
	unsigned int datasize=signal->size();
	for(unsigned int i=0;i<datasize;i++){
		float t=(float)i/(float)pps;
		fs<<t<<","<<(*signal)[i]<<endl;
	}

	fs.close();

}

